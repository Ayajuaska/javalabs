import org.junit.Assert;
import org.junit.Test;

public class GenericSwapTest {
    @Test
    public void testString(){
        String a = "aaaa";
        String b = "bbbbb";
        String[] input = new String[]{a, "cccc", b};
        Assert.assertTrue(GenericSwap.swap(0, 2, input));
        Assert.assertEquals(a, input[2]);
        Assert.assertEquals(b, input[0]);
    }

    @Test
    public void testOutOfBounds() {
        Integer[] input = new Integer[0];
        Assert.assertFalse(GenericSwap.swap(1, 3, input));
    }
}
