class GenericSwap {
    private final static Object sync = new Object();
    static <T> boolean swap(int a, int b, T[] input) {
        if (a >= input.length || b >= input.length) {
            return false;
        }
        synchronized (sync) {
            T tmp = input[a];
            input[a] = input[b];
            input[b] = tmp;
        }
        return true;
    }
}
