import org.junit.Assert;
import org.junit.Test;

/**
 * "aab" -> "b", "aabb" -> "", "abfbaf" -> ""abfbaf”, "abccbaf" -> "f"
 */
public class DeDuplicatorTest {

    @Test
    public void TestStack() {
        Stack<String> stack = new Stack<String>();
    }

    @Test
    public void TestEmpty() {
        testBasic("", 0);
        boolean caught = false;
        Stack<String> stack = new Stack<String>();
        try {
            stack.last();
        } catch (IndexOutOfBoundsException err) {
            caught = true;
        }
        Assert.assertTrue(caught);
    }

    @Test
    public void TestAABB() {
        testBasic("aabb", 0);
    }

    @Test
    public void TestAAB() {
        testBasic("aab", 1);
    }

    @Test
    public void TestABFBAF() {
        testBasic("abfbaf", 6);
    }

    @Test
    public void TestABCCBAF() {
        testBasic("abccbaf", 1);
    }

    private void testBasic(String s, int length) {
        String result = DeDuplicator.run(s);
        Assert.assertEquals(length, result.length());
    }
}
