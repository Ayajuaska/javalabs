/**
 * Надо реализовать метод, который принимает на вход String, удаляет рядом стоящие парные буквы и возвращает остаток. Примеры ожидаемого поведения программы:
 * "aab" -> "b", "aabb" -> "", "abfbaf" -> ""abfbaf”, "abccbaf" -> "f"
 * Реализовать метод и описать его алгоритмическую сложность. Напишите junit-тесты.
 */
class DeDuplicator {
    static String run(String input) {
        Stack<String> stack = new Stack<String>(input.length());
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            String s = "";
            s += input.charAt(i);
            if (stack.size() == 0) {
                stack.push(s);
                continue;
            }
            if (!stack.last().equals(s)) {
                stack.push(s);
            } else {
                stack.pop();
            }
        }
        String s;
        while (true) {
            try {
                s = stack.pop();
            } catch (IndexOutOfBoundsException exc) {
                break;
            }
            result.append(s);
        }
        return result.toString();
    }
}
