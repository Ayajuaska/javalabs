import java.util.ArrayList;

class Stack<T> {
    private ArrayList<T> container;

    Stack(int size) {
        this.container = new ArrayList<T>();
        this.container.ensureCapacity(size);
    }

    Stack() {
        this.container = new ArrayList<T>();
    }

    int size() {
        return container.size();
    }

    T pop() throws IndexOutOfBoundsException {
        if (container.size() < 1) {
            throw new IndexOutOfBoundsException();
        }
        return container.remove(container.size() - 1);
    }

    void push(T val) {
        container.add(val);
    }

    T last() throws IndexOutOfBoundsException {
        if (container.size() < 1) {
            throw new IndexOutOfBoundsException();
        }
        return container.get(container.size() - 1);
    }
}
