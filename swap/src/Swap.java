public class Swap {
    private final static Object sync = new Object();

    public static boolean swap(int a, int b, int[] input) {
        if (a >= input.length || b >= input.length) {
            return false;
        }
        synchronized (sync) {
            int tmp = input[a];
            input[a] = input[b];
            input[b] = tmp;
        }
        return true;
    }
}
