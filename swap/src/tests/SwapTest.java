import org.junit.Assert;
import org.junit.Test;

public class SwapTest {

    @Test
    public void TestEmpty() {
        int[] empty = new int[0];
        Assert.assertFalse(Swap.swap(1, 2, empty));
    }

    @Test
    public void TestSwap() {
        int[] array = new int[] {1, 2, 3};
        Swap.swap(0, 2, array);
        Assert.assertEquals(3, array[0]);
        Assert.assertEquals(1, array[2]);
    }
}
