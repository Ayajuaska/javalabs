public class Spaceship {
    private float __speed = 0;
    static float max_speed = 300 * 1000 * 1000;
    Spaceship(float speed) {
        if (speed <= Spaceship.max_speed)
            this.__speed = speed;
        else
            this.__speed = max_speed;
    }

    Spaceship() {}

    public float getSpeed() { return this.__speed; }
}
