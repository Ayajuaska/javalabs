import java.util.ArrayList;


public class SpaceFleet {
    private ArrayList<Spaceship> __ships = new ArrayList<>();
    public SpaceFleet() {

    }
    public SpaceFleet(int size) {
        for (int i = 0; i < size; i++) {
            this.__ships.add(new Spaceship());
        }
    }

    public void AddShip(float speed) {
        this.__ships.add(new Spaceship(speed));
    }

    /**
     * Fleet's speed is the average speed of all its ships
     * @return avg speed of ships
     */
    public float getSpeed() {
        float speed = 0;
        if (this.__ships.size() == 0) {
            return speed;
        }
        for (Spaceship ship : this.__ships) {
            speed += ship.getSpeed();
        }
        speed = speed / this.__ships.size();
        return speed;
    }

    public float MinSpeed() {
        if (this.__ships.isEmpty()) {
            return 0;
        }
        float min_speed = this.__ships.get(0).getSpeed();
        for (Spaceship ship : this.__ships) {
            if (ship.getSpeed() < min_speed) {
                min_speed = ship.getSpeed();
            }
        }
        return min_speed;
    }

    public float MaxSpeed() {
        if (this.__ships.isEmpty()) {
            return 0;
        }
        float max_speed = this.__ships.get(0).getSpeed();
        for (Spaceship ship : this.__ships) {
            if (ship.getSpeed() > max_speed) {
                max_speed = ship.getSpeed();
            }
        }
        return max_speed;
    }

    public int getShipsIntFleet() {
        return __ships.size();
    }
}
