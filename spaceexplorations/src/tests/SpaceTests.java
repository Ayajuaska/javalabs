import org.junit.Assert;
import org.junit.Test;

public class SpaceTests {
    private int initial_size = 3;

    @Test
    public void creationTest() {
        SpaceFleet fleet = new SpaceFleet(initial_size);
        Assert.assertEquals(initial_size, fleet.getShipsIntFleet());
    }

    @Test
    public void fleetExpansionTest() {
        SpaceFleet fleet = new SpaceFleet();
        fleet.AddShip(0);
        Assert.assertEquals(1, fleet.getShipsIntFleet());
    }

    @Test
    public void avgSpeedTest()
    {
        SpaceFleet fleet = new SpaceFleet(initial_size);
        int speed = Math.round(fleet.getSpeed());
        Assert.assertEquals(0, speed);
        fleet = new SpaceFleet();
        fleet.AddShip(3);
        fleet.AddShip(7);
        speed = Math.round(fleet.getSpeed());
        Assert.assertEquals(5, speed);
        fleet = new SpaceFleet();
        speed = Math.round(fleet.getSpeed());
        Assert.assertEquals(0, speed);
    }

    @Test
    public void maxSpeedTest()
    {
        SpaceFleet fleet = new SpaceFleet();
        fleet.AddShip(3);
        fleet.AddShip(7);
        int max_speed = Math.round(fleet.MaxSpeed());
        System.out.println(fleet.MaxSpeed());
        Assert.assertEquals(7, max_speed);

        fleet = new SpaceFleet();
        fleet.AddShip(7);
        fleet.AddShip(3);
        max_speed = Math.round(fleet.MaxSpeed());
        System.out.println(fleet.MaxSpeed());
        Assert.assertEquals(7, max_speed);

        fleet = new SpaceFleet();
        max_speed = Math.round(fleet.MaxSpeed());
        Assert.assertEquals(0, max_speed);
    }

    @Test
    public void minSpeedTest()
    {
        SpaceFleet fleet = new SpaceFleet();
        fleet.AddShip(3);
        fleet.AddShip(7);
        int min_speed = Math.round(fleet.MinSpeed());
        Assert.assertEquals(3, min_speed);

        fleet = new SpaceFleet();
        fleet.AddShip(7);
        fleet.AddShip(3);
        min_speed = Math.round(fleet.MinSpeed());
        Assert.assertEquals(3, min_speed);


        fleet = new SpaceFleet();
        min_speed = Math.round(fleet.MinSpeed());
        Assert.assertEquals(0, min_speed);
    }

    @Test
    public void hyperLightSpeedTest()
    {
        Spaceship ship = new Spaceship(1234567890);
        int real_speed = Math.round(ship.getSpeed());
        int max_speed = Math.round(Spaceship.max_speed);
        Assert.assertEquals(max_speed, real_speed);
    }
}
