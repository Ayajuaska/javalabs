import org.junit.Assert;
import org.junit.Test;
import java.lang.ArithmeticException;


public class SummTest {
    private final Calculator calculator = new Calculator();
    @Test
    public void checkBasic() {
        Assert.assertEquals(2, calculator.summ(1, 1));
    }

    @Test
    public void checkZero() {
        Assert.assertEquals(0, calculator.summ(-1, 1));
    }

    @Test
    public void checkOverFlow() {
        boolean result = false;
        try {
            calculator.summ(2147483647, 2147483647);
        } catch (ArithmeticException err) {
            result = true;
        }
        Assert.assertTrue(result);
    }
/*
    @Test
    public void dummyTest() {
        boolean result = false;
        Main tmp = new Main();
        Assert.assertEquals(tmp.getClass().getName(), "Main");
        try {

            Main.main(new String[0]);
        } catch (ArithmeticException err) {
            result = true;
        }
        Assert.assertTrue(result);
    }
     */
}
