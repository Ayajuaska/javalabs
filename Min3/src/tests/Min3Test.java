import org.junit.Assert;
import org.junit.Test;

public class Min3Test {

    @Test
    public void TestA() {
        Assert.assertEquals(-23, Min3.min3(-23, 3, 1));
    }

    @Test
    public void TestB() {
        Assert.assertEquals(-1233, Min3.min3(-23, -1233, 1));
    }

    @Test
    public void TestC() {
        Assert.assertEquals(1, Min3.min3(2, 3, 1));
    }

    @Test
    public void TestAEquals() {
        Assert.assertEquals(3, Min3.min3(3, 3, 3));
    }
}
